module.exports = {
  apps: [
    {
      name: 'team-bu',
      script: './node_modules/nuxt-start/bin/nuxt-start.js',
      instances: 'max', //負載平衡下的cpu數量
      exec_mode: 'cluster', //cpu 負載平衡模式
      max_memory_restart: '1G', //緩存多少記憶體重整
      port: 3000 //指定伺服器上的port
    }
  ],

  // deploy : {
  //   production : {
  //     user : 'SSH_USERNAME',
  //     host : 'SSH_HOSTMACHINE',
  //     ref  : 'origin/master',
  //     repo : 'GIT_REPOSITORY',
  //     path : 'DESTINATION_PATH',
  //     'pre-deploy-local': '',
  //     'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
  //     'pre-setup': ''
  //   }
  // }
};
